import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import React from "react";
import {useMutation} from "@apollo/react-hooks";
import {createUserMutation} from "./Mutation";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function SignUp(props) {
  const {setOpenDialog} = props

  const [name, setName] = React.useState(null);
  const [email, setEmail] = React.useState(null)
  const [password, setPassword] = React.useState(null)
  const [open, setOpen] = React.useState(false);
  const [createUser,] = useMutation(createUserMutation)

  const handleClose = (event, reason) => {
    setOpen(false)
    setOpenDialog(false)
  };

  return(
    <div>
      <form onSubmit={e => {
        e.preventDefault()
        createUser({variables: {name: name, auth: {email: email, password: password}}}).then(r => {
          setOpen(true)
        })
      }}>
        <p>
          <TextField
            id = 'name'
            label = 'name'
            value = {name}
            required
            fullWidth={true}
            onChange={e => {
              setName(e.target.value)
            }}
          >
          </TextField>
        </p>
        <p>
          <TextField
            id = 'email'
            label = 'email'
            value = {email}
            required
            fullWidth={true}
            onChange={e => {
              setEmail(e.target.value)
            }}
          >
          </TextField>
        </p>
        <p>
          <TextField
            id = 'password'
            label = 'password'
            value = {password}
            type="password"
            required
            fullWidth={true}
            onChange={e => {
              setPassword(e.target.value)
            }}
          >
          </TextField>
        </p>
        <p>
          <Button
            type="submit"
            color="primary"
            variant="contained">
            Save
          </Button>
        </p>
      </form>
      <Snackbar open={open} autoHideDuration={1000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          Пользователь успешно добавлен
        </Alert>
      </Snackbar>
    </div>
  )
}

export default SignUp
