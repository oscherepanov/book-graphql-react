import {gql} from "apollo-boost";

export const createUserMutation = gql `
  mutation CreateUser($name: String!, $auth: AuthData){
    createUser(name: $name, auth: $auth){
      name
    }
  }
`
