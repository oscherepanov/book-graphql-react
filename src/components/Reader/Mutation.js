import {gql} from 'apollo-boost';

export const addReaderMutation = gql `
  mutation AddReader($request: ReaderCreateRequest!){
    createReader(reader: $request){
      id
      name
    }
  }
`
export const updateReaderMutation = gql `
  mutation UpdateReader($request: ReaderEditRequest!){
    updateReader(reader: $request){
      id
      name
    }
  }
`

export const deleteReaderMutation = gql `
  mutation DeleteReaderMutation($id: ID!){
    deleteReader(id: $id)
  }
`

