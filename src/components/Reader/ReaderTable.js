import {allReaderQuery} from "./Query";
import React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import Table from "@material-ui/core/Table";
import {TableBody} from "@material-ui/core";
import TableRow from "@material-ui/core/TableRow";
import Fab from "@material-ui/core/Fab";
import AddIcon from '@material-ui/icons/Add';
import {useMutation, useQuery} from "@apollo/react-hooks";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Popup from "../utils/Popup";
import ReaderForm from "./ReaderForm";
import {addReaderMutation, deleteReaderMutation, updateReaderMutation} from "./Mutation";
import Button from "@material-ui/core/Button";
import {Delete, Edit} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  }
}));

function ReaderTable() {
  const handleOpenDialog = (item) => {
    setReaderForEdit(item)
    setOpenDialog(true)
  }

  const saveReaderHandle = (reader) => {
    console.log(reader)
    if (reader['id'] === 0){

      addReader({variables: {request: {name: reader['name']}},
        refetchQueries: [{query: allReaderQuery}]})
    }
    else{
      updateReader({variables: {request: {id: reader['id'], name: reader['name']}}, refetchQueries: [{query: allReaderQuery}]})
    }
    setOpenDialog(false)
  }

  const deleteReaderHandle = (id) => {
    delReader({variables: {id: id}, refetchQueries: [{query: allReaderQuery}]})
  }

  const [openDialog, setOpenDialog] = React.useState(0)
  const [addReader, {add_data}] = useMutation(addReaderMutation)
  const [updateReader, {edit_data}] = useMutation(updateReaderMutation)
  const [delReader, {del_data}] = useMutation(deleteReaderMutation)
  const [ReaderForEdit, setReaderForEdit] = React.useState(null)

  const classes = useStyles();
  const { loading, error, data } = useQuery(allReaderQuery);
  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
     <div>
       <h1> Читатели </h1>
       <Table>
            <TableHead>
              <TableRow>
                <TableCell> id </TableCell>
                <TableCell> ФИО </TableCell>
                <TableCell> Действия </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.allReaders.map(({id, name}) => (
                <TableRow key={id}>
                  <TableCell> {id} </TableCell>
                  <TableCell> {name} </TableCell>
                  <TableCell>
                    <Button color="primary" onClick={() => {handleOpenDialog({id: id, name: name})}}>
                      <Edit/>
                    </Button>
                    <Button color="secondary" onClick={() => {deleteReaderHandle(id)}}>
                      <Delete/>
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <Fab color='primary' aria-label='Add' className={classes.fab} onClick={
            () => {handleOpenDialog({id: 0, name:""})}
          }>
            <AddIcon/>
          </Fab>
          <Popup
            openPopup = {openDialog}
            setOpenPopup = {setOpenDialog}
            title = {'Редактирование читателя'}
          >
            <ReaderForm
              saveHandle = {saveReaderHandle}
              reader = {ReaderForEdit}
            />
          </Popup>
        </div>
      )
}

export default ReaderTable;
