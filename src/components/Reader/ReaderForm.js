import React from "react";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";

function ReaderForm(props) {
  const {reader, saveHandle} = props
  const [name, setName] = React.useState(reader["name"]);

  return (
    <div>
    <form onSubmit={e => {
      e.preventDefault()
      saveHandle({id: parseInt(reader["id"]), name: name})
    }}>
      <p>
        <TextField
          id = 'name'
          label = 'name'
          value = {name}
          onChange={e => {
            setName(e.target.value)
          }}
        >
        </TextField>
      </p>
      <p>
        <Button
          type="submit"
          color="primary"
          variant="contained">
          Save
        </Button>
      </p>
    </form>
    </div>
  )
}

export default ReaderForm;
