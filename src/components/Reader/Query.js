import {gql} from 'apollo-boost';

export const allReaderQuery = gql `
  query {
    allReaders {
      id
      name
    }
  }
`
