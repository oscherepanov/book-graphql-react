import React, {useState} from "react";
import {uploadFile} from "./Mutations";
import {useMutation} from "@apollo/react-hooks";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import {Inbox} from "@material-ui/icons";

function FileUploadForm() {
  const [urls, setUrls] = useState([])
  const [upload] = useMutation(uploadFile, {onCompleted: data => {
      setUrls(data.uploadFiles)
    }});

  return (
    <div>
      <h1> Загрузка файлов</h1>
      <input type="file" placeholder="Choose a file"
        onChange={({target: {files: [file]}}) => {
          upload({variables: {file: file}})
          }
        }/>
        <List>
        {urls.map((item, i) => (
          <ListItem  button component="a" href={item}>
            <ListItemIcon>
              <Inbox />
            </ListItemIcon>
            <ListItemText primary={item} />
          </ListItem>
        ))}
        </List>
      </div>
    )
}

export default FileUploadForm
