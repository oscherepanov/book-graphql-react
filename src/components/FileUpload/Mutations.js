import {gql} from 'apollo-boost';

export const uploadFile = gql `
  mutation ($files: [Upload!]){
    uploadFiles(files: $files)
  }
`
