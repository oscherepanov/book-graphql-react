import {useSubscription} from "@apollo/react-hooks";
import {bookSubscription} from "./Subscriptions";
import React from "react";

function BookSubscription(){
  const {data, error, loading} = useSubscription(bookSubscription)

  if (loading) {return <p> Loading... </p>}
  if (error) {return <p> Error: {error.message}</p>}

  return (
    <div>
      <p> ID: {data.newBook.id}</p>
      <p> Name: {data.newBook.name} </p>
      <p> Year: {data.newBook.year} </p>
      <p> Genre: {data.newBook.genre.name}</p>
      <p> Authors:
        <ul>
          {data.newBook.authors.map(({name}) => (
            <li> {name} </li>
          ))}
        </ul>
      </p>
    </div>
  )
}

export default BookSubscription
