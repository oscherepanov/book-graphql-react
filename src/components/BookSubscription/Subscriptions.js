import {gql} from "apollo-boost";

export const bookSubscription = gql `
  subscription {
    newBook{
      id
      name
      year
      genre {
        name
      }
      authors{
        name
      }
    }
  }
`
