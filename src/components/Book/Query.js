import {gql} from 'apollo-boost';

export const allBookQuery = gql `
  query {
    allBooks {
      id
      name
      year
      genre {
        id
        name
      }
      authors{
        id
        name
      }
      journals{
        id
        created 
      }
    }
  }
`