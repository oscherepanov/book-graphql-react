import React from "react";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import {useQuery} from "@apollo/react-hooks";
import {allGenreQuery} from "../Genre/Query";
import TransferList from "../common/TransferList"
import makeStyles from "@material-ui/core/styles/makeStyles";
import {allAuthorsQuery} from "../Author/Query";


const useStyles = makeStyles((theme) => ({
  form: {
    width: 400,
    height: 350,
  }
}));

function BookForm(props) {
  const classes = useStyles();
  const {book, saveHandle} = props;
  const [name, setName] = React.useState(book["name"]);
  const [year, setYear] = React.useState(book["year"]);
  const [genre, setGenre] = React.useState(book["genre"]);
  const [authors, setAuthors] = React.useState(book["authors"]);

  const {loading, error, data } = useQuery(allGenreQuery);
  const allAuthor = useQuery(allAuthorsQuery);

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;
  if (allAuthor.loading) return 'Loading...';
  if (allAuthor.error) return `Error! ${allAuthor.error.message}`;

  let left = allAuthor.data.allAuthors.filter((el) => !authors.map((t) => t.id ).includes(el.id));
  let right = authors;
  let item = {left, right}

  return (
    <div className={classes.form}>
    <form onSubmit={e => {
      e.preventDefault()
      saveHandle({id: parseInt(book["id"]), name: name, year: year, genre_id: parseInt(genre), author_ids: authors.map((e)=> parseInt(e.id))})
    }}>
      <TransferList
          item = {item}
          setAuthors = {setAuthors}
      />
      <p>
        <TextField
          id = 'name'
          label = 'name'
          value = {name}
          onChange={e => {
            setName(e.target.value)
          }}
        >
        </TextField>
      </p>
      <p>
        <TextField
            id = 'year'
            label = 'year'
            value = {year}
            onChange={e => {
              setYear(e.target.value)
            }}
        >
        </TextField>
      </p>
      <p>
        <Select
            id = 'genre'
            label='genre'
            value={genre}
            onChange={(e) => {
              setGenre(e.target.value)
            }}>
          {data.allGenres.map(({id, name}) => (
              <MenuItem value={id}> {name} </MenuItem>
          ))}
        </Select>
      </p>
      <p>
        <Button
          type="submit"
          color="primary"
          variant="contained">
          Save
        </Button>
      </p>
    </form>
    </div>
  )
}

export default BookForm;
