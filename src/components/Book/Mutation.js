import {gql} from 'apollo-boost';

export const addBookMutation = gql `
  mutation AddBook($request: BookCreateRequest!){
    createBook(book: $request){
       id
       name
       year
       genre{
        id
      }
      authors {
        id
      }
    }
  }
`
export const updateBookMutation = gql `
  mutation UpdateBook($request: BookEditRequest!){
    updateBook(book: $request){
      id
      name
       year
       genre{
        id
      }
      authors {
        id
      }
    }
  }
`

export const deleteBookMutation = gql `
  mutation DeleteBook($id: ID!){
    deleteBook(id: $id)
  }
`

