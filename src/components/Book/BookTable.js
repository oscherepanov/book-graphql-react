import {allBookQuery} from "./Query";
import React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import Table from "@material-ui/core/Table";
import {TableBody} from "@material-ui/core";
import TableRow from "@material-ui/core/TableRow";
import Fab from "@material-ui/core/Fab";
import AddIcon from '@material-ui/icons/Add';
import {useMutation, useQuery} from "@apollo/react-hooks";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Popup from "../utils/Popup";
import BookForm from "./BookForm";
import {addBookMutation, deleteBookMutation, updateBookMutation} from "./Mutation";
import Button from "@material-ui/core/Button";
import {Delete, Edit} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  }
}));

function BookTable() {
  const handleOpenDialog = (item) => {
    setBookForEdit(item)
    setOpenDialog(true)
  }

  const saveBookHandle = (book) => {
      console.log(book)
    if (book['id'] === 0){
      addBook({variables: {request: {name: book['name'], year: book['year'], genre_id: book['genre_id'], author_ids: book['author_ids']}},
        refetchQueries: [{query: allBookQuery}]})
    }
    else{
      updateBook({variables: {request: {id: book['id'], name: book['name'], year: book['year'], genre_id: book['genre_id'], author_ids: book['author_ids']}}, refetchQueries: [{query: allBookQuery}]})
    }
    setOpenDialog(false)
  }

  const deleteBookHandle = (id) => {
    delBook({variables: {id: id}, refetchQueries: [{query: allBookQuery}]})
  }

  const [openDialog, setOpenDialog] = React.useState(0)
  const [addBook, {add_data}] = useMutation(addBookMutation)
  const [updateBook, {edit_data}] = useMutation(updateBookMutation)
  const [delBook, {del_data}] = useMutation(deleteBookMutation)
  const [BookForEdit, setBookForEdit] = React.useState(null)

  const classes = useStyles();
  const { loading, error, data } = useQuery(allBookQuery);
  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  const concatArrayItem = (array) => {
      const result = array.map(({id, name})=> name).join(', ');
      return result;
  }

  return (
     <div>
       <h1> Книги </h1>
       <Table>
            <TableHead>
              <TableRow>
                <TableCell> id </TableCell>
                <TableCell> Наименование </TableCell>
                <TableCell> Год </TableCell>
                <TableCell> Жанр </TableCell>
                <TableCell> Авторы </TableCell>
                <TableCell> Действия </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.allBooks.map(({id, name, year, genre, authors}) => (
                <TableRow key={id}>
                  <TableCell> {id} </TableCell>
                  <TableCell> {name} </TableCell>
                  <TableCell> {year} </TableCell>
                  <TableCell> {genre.name} </TableCell>
                  <TableCell> {concatArrayItem(authors)} </TableCell>
                  <TableCell>
                    <Button color="primary" onClick={() => {handleOpenDialog({id, name, year, genre: genre["id"], authors })}}>
                      <Edit/>
                    </Button>
                    <Button color="secondary" onClick={() => {deleteBookHandle(id)}}>
                      <Delete/>
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <Fab color='primary' aria-label='Add' className={classes.fab} onClick={
            () => {handleOpenDialog({id: 0, name:"",  year: "", genre: 0, authors: []})}
          }>
            <AddIcon/>
          </Fab>
          <Popup
            openPopup = {openDialog}
            setOpenPopup = {setOpenDialog}
            title = {'Редактирование книги'}
          >
            <BookForm
              saveHandle = {saveBookHandle}
              book = {BookForEdit}
            />
          </Popup>
        </div>
      )
}

export default BookTable;
