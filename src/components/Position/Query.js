import {gql} from 'apollo-boost';

export const allPositionQuery = gql `
  query {
    allPositions {
      id
      name
    }
  }
`
