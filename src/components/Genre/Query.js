import {gql} from 'apollo-boost';

export const allGenreQuery = gql `
  query {
    allGenres {
      id
      name
    }
  }
`
