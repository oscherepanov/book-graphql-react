import makeStyles from "@material-ui/core/styles/makeStyles";
import {useMutation, useQuery} from "@apollo/react-hooks";
import {useState} from "react";
import {deleteJournalMutation} from "./Mutation";
import {allJournalsQuery} from "./Query";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Button from "@material-ui/core/Button";
import {Delete, Edit} from "@material-ui/icons";
import Fab from "@material-ui/core/Fab";
import AddIcon from '@material-ui/icons/Add';
import Popup from "../utils/Popup";
import JournalForm from "./JournalForm";


const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  }
}));

function JournalTable() {
  const classes = useStyles();

  const [openDialog, setOpenDialog] = useState(0)
  const [deleteCourse,] = useMutation(deleteJournalMutation)
  const [recordIdForEdit, setRecordIdForEdit] = useState(null)

  const { loading, error, data } = useQuery(allJournalsQuery);
  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
     <div>
       <h1> Журнал выдачи/возврата книг </h1>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell> id </TableCell>
                <TableCell> Книга </TableCell>
                <TableCell> Читатель </TableCell>
                <TableCell> Действие </TableCell>
                <TableCell> Дата </TableCell>
                <TableCell> </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.getJournal.map(({id, book, reader, action, created}) => (
                <TableRow key={id}>
                  <TableCell> {id} </TableCell>
                  <TableCell> {book['name']} </TableCell>
                  <TableCell> {reader['name']} </TableCell>
                  <TableCell> {action} </TableCell>
                  <TableCell> {created} </TableCell>
                  <TableCell>
                    <Button color="primary" onClick={() => {
                      setRecordIdForEdit(id)
                      setOpenDialog(true)
                    }}>
                      <Edit/>
                    </Button>
                    <Button color="secondary" onClick={() => {
                    deleteCourse({variables: {id: id}, refetchQueries: [{query: allJournalsQuery}]})
                    }}>
                    <Delete/>
                  </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
       <Fab color='primary' aria-label='Add' className={classes.fab} onClick={
         () => {
           setRecordIdForEdit('0')
           setOpenDialog(true)
         }
       }>
         <AddIcon/>
       </Fab>
       <Popup
         openPopup = {openDialog}
         setOpenPopup = {setOpenDialog}
         title = {'Редактирование курса'}
       >
         <JournalForm
           journal_id = {recordIdForEdit}
           setOpenDialog = {setOpenDialog}
         />
       </Popup>
        </div>
      )
}

export default JournalTable;
