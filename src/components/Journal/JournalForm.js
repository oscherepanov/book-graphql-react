import React, {useState} from "react";
import {useMutation, useQuery} from "@apollo/react-hooks";
import {addJournalMutation, updateJournalMutation} from "./Mutation";
import {allJournalsQuery, findJournalRecordQuery} from "./Query";
import {allReaderQuery} from "../Reader/Query";
import {allEmployeesQuery} from "../Employee/Query";
import {allBookQuery} from "../Book/Query";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";


function JournalForm(props) {
  const {journal_id, setOpenDialog} = props

  const [bookId, setBookId] = useState(0);
  const [readerId, setReaderId] = useState(0)
  const [employeeId, setEmployeeID] = useState(0)
  const [action, setAction] = useState('GIVE_OUT')

  const currentDate = new Date();
  let created = currentDate.toISOString();
  console.log(created)

  const [addJournal,] = useMutation(addJournalMutation)
  const [updateJournal,] = useMutation(updateJournalMutation)

  useQuery(findJournalRecordQuery, {variables: {id: journal_id},
    skip: journal_id === '0', onCompleted: data => {
      if (journal_id !== '0') {
        setBookId(data.findJournalRecord.book.id)
        setReaderId(data.findJournalRecord.reader.id)
        setEmployeeID(data.findJournalRecord.employee.id)
        setAction(data.findJournalRecord.action)
        created =data.findJournalRecord.created
      }
    } });

  const { loading: readers_loading, error: readers_error, data: data_readers } = useQuery(allReaderQuery);
  const { loading: book_loading, error: book_error, data: books_data } = useQuery(allBookQuery);
  const { loading: emp_loading, error: emp_error, data: data_emps } = useQuery(allEmployeesQuery);

  if (book_loading || readers_loading || emp_loading)
    return "Loading..."
  if (readers_error) return `Error! ${readers_error.message}`;
  if (book_error) return `Error! ${book_error.message}`;
  if (emp_error) return `Error! ${emp_error.message}`;

  return (
    <div>
      <form onSubmit={e => {
        e.preventDefault()
        if (journal_id === '0'){
          addJournal({variables: {request: {book_id: bookId, reader_id: readerId,
              employee_id: employeeId, action: action, created: created}},
            refetchQueries: [{query: allJournalsQuery}]})
        }
        else{
          updateJournal({variables: {request: {id: journal_id, book_id: bookId, reader_id: readerId,
            employee_id: employeeId, action: action, created: created}}, refetchQueries: [{query: allJournalsQuery}]})
        }
        setOpenDialog(false)
      }}>
        <p>
          <Select
            id = 'reader'
            label='reader'
            value={readerId}
            fullWidth={true}
            onChange={(e) => {
              setReaderId(e.target.value)
            }}>
            {data_readers.allReaders.map(({id, name}) => (
              <MenuItem value={id}> {name} </MenuItem>
            ))}
          </Select>
        </p>
        <p>
          <Select
            id = 'book'
            label='book'
            value={bookId}
            fullWidth={true}
            onChange={(e) => {
              setBookId(e.target.value)
            }}>
            {books_data.allBooks.map(({id, name}) => (
              <MenuItem value={id}> {name} </MenuItem>
            ))}
          </Select>
        </p>
        <p>
          <Select
            id = 'employee'
            label='employee'
            value={employeeId}
            fullWidth={true}
            onChange={(e) => {
              setEmployeeID(e.target.value)
            }}>
            {data_emps.allEmployees.map(({id, name}) => (
              <MenuItem value={id}> {name} </MenuItem>
            ))}
          </Select>
        </p>
        <p>
          <Select
            id = 'action'
            label='action'
            value={action}
            fullWidth={true}
            onChange={(e) => {
              setAction(e.target.value)
            }}>
            <MenuItem value='GIVE_OUT'> Выдать </MenuItem>
            <MenuItem value='PICK_UP'> Принять </MenuItem>
          </Select>
        </p>
        <p>
          <Button
            type="submit"
            color="primary"
            variant="contained">
            Сохранить
          </Button>
        </p>
      </form>
    </div>
  )
}

export default JournalForm;
