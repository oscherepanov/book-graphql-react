import {gql} from 'apollo-boost';

export const addJournalMutation = gql `
  mutation AddJournal($request: JournalCreateRequest!){
    createJournal(journal: $request){
      id
    }
  }
`
export const updateJournalMutation = gql `
  mutation UpdateJournal($request: JournalEditRequest!){
    updateJournal(journal: $request){
      id
    }
  }
`

export const deleteJournalMutation = gql `
  mutation DeleteJournalMutation($id: ID!){
    deleteJournal(id: $id)
  }
`

