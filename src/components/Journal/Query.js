import {gql} from 'apollo-boost';

export const allJournalsQuery = gql `
  query {
    getJournal {
      id
      book {
        name
      }
      reader {
        name
      }
      action
      created
    }
  }
`

export const findJournalRecordQuery = gql `
  query FindRecord($id: ID!){
    findJournalRecord(id: $id) {
      id
      book {
        id
      }
      reader {
        id
      }
      employee {
        id
      }
      action
      created
    }
  }
`
