import {gql} from 'apollo-boost';

export const addAuthorMutation = gql `
  mutation AddAuthor($request: AuthorCreateRequest!){
    createAuthor(author: $request){
      id
      name
    }
  }
`
export const updateAuthorMutation = gql `
  mutation UpdateAuthor($request: AuthorEditRequest!){
    updateAuthor(author: $request){
      id
      name
    }
  }
`

export const deleteAuthorMutation = gql `
  mutation DeleteAuthorMutation($id: ID!){
    deleteAuthor(id: $id)
  }
`

