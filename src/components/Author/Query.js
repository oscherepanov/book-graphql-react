import {gql} from 'apollo-boost';

export const allAuthorsQuery = gql `
  query {
    allAuthors {
      id
      name
    }
  }
`
