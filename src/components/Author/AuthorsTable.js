import {allAuthorsQuery} from "./Query";
import React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import Table from "@material-ui/core/Table";
import {TableBody} from "@material-ui/core";
import TableRow from "@material-ui/core/TableRow";
import Fab from "@material-ui/core/Fab";
import AddIcon from '@material-ui/icons/Add';
import {useMutation, useQuery} from "@apollo/react-hooks";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Popup from "../utils/Popup";
import AuthorForm from "./AuthorForm";
import {addAuthorMutation, deleteAuthorMutation, updateAuthorMutation} from "./Mutation";
import Button from "@material-ui/core/Button";
import {Delete, Edit} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  }
}));

function AuthorsTable() {
  const handleOpenDialog = (item) => {
    setAuthorForEdit(item)
    setOpenDialog(true)
  }

  const saveAuthorHandle = (author) => {
    if (author['id'] === '0'){
      addAuthor({variables: {request: {name: author['name']}}, refetchQueries: [{query: allAuthorsQuery}]})
    }
    else{
      updateAuthor({variables: {request: {id: author['id'], name: author['name']}},
        refetchQueries: [{query: allAuthorsQuery}]})
    }
    setOpenDialog(false)
  }

  const deleteAuthorHandle = (id) => {
    delAuthor({variables: {id: id}, refetchQueries: [{query: allAuthorsQuery}]})
  }

  const [openDialog, setOpenDialog] = React.useState(0)
  const [addAuthor, {add_data}] = useMutation(addAuthorMutation)
  const [updateAuthor, {edit_data}] = useMutation(updateAuthorMutation)
  const [delAuthor, {del_data}] = useMutation(deleteAuthorMutation)
  const [authorForEdit, setAuthorForEdit] = React.useState(null)

  const classes = useStyles();
  const { loading, error, data } = useQuery(allAuthorsQuery);
  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
     <div>
       <h1> Авторы </h1>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell> id </TableCell>
                <TableCell> name </TableCell>
                <TableCell> action </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.allAuthors.map(({id, name,}) => (
                <TableRow key={id}>
                  <TableCell> {id} </TableCell>
                  <TableCell> {name} </TableCell>
                  <TableCell>
                    <Button color="primary" onClick={() => {handleOpenDialog({id: id, name: name})}}>
                      <Edit/>
                    </Button>
                    <Button color="secondary" onClick={() => {deleteAuthorHandle(id)}}>
                      <Delete/>
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <Fab color='primary' aria-label='Add' className={classes.fab} onClick={
            () => {handleOpenDialog({id: '0', name:""})}
          }>
            <AddIcon/>
          </Fab>
          <Popup
            openPopup = {openDialog}
            setOpenPopup = {setOpenDialog}
            title = {'Редактирование автора'}
          >
            <AuthorForm
              saveHandle = {saveAuthorHandle}
              author = {authorForEdit}
            />
          </Popup>
        </div>
      )
}

export default AuthorsTable;
