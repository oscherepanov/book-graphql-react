import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import React from "react";
import Button from "@material-ui/core/Button";
import {Close} from "@material-ui/icons";
import Box from "@material-ui/core/Box";
import Divider from "@material-ui/core/Divider";

export default function Popup(props){
  const {title, openPopup, setOpenPopup, children} = props

  return (
    <Dialog
      open={openPopup}
      maxWidth={'lg'}
      fullWidth={true}
    >
      <DialogTitle>
        <Box display="flex" p={1}>
          <Box p={1} width="100%">
            {title}
          </Box>
          <Box p={1} flexShrink={1}>
            <Button color="secondary"
                    onClick={() => {setOpenPopup(false)}}>
              <Close/>
            </Button>
          </Box>
        </Box>
        <Divider />
      </DialogTitle>
      <DialogContent>
        {children}
      </DialogContent>
    </Dialog>
  )
}
