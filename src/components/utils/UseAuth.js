const {useEffect} = require("react");
const {useState} = require("react");

function useAuth(defaultValue) {
  const key = "authData"
  const [value, setValue] = useState(() => {
    const value = window.localStorage.getItem(key);
    return value !== null
      ? JSON.parse(value)
      : defaultValue;
  });
  useEffect(() => {
    window.localStorage.setItem("authData", JSON.stringify(value))});
  return [value, setValue];
}

export default useAuth
