import {gql} from 'apollo-boost';

export const addEmployeeMutation = gql `
  mutation AddEmployee($request: EmployeeCreateRequest!){
    createEmployee(employee: $request){
      id
      name
      position {
        id
      }
    }
  }
`
export const updateEmployeeMutation = gql `
  mutation UpdateEmployee($request: EmployeeEditRequest!){
    updateEmployee(employee: $request){
      id
      name
      position {
        id
      }
    }
  }
`

export const deleteEmployeeMutation = gql `
  mutation DeleteEmployeeMutation($id: ID!){
    deleteEmployee(id: $id)
  }
`

