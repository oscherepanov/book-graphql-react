import {allEmployeesQuery} from "./Query";
import React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import Table from "@material-ui/core/Table";
import {TableBody} from "@material-ui/core";
import TableRow from "@material-ui/core/TableRow";
import Fab from "@material-ui/core/Fab";
import AddIcon from '@material-ui/icons/Add';
import {useMutation, useQuery} from "@apollo/react-hooks";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Popup from "../utils/Popup";
import EmployeeForm from "./EmployeeForm";
import {addEmployeeMutation, deleteEmployeeMutation, updateEmployeeMutation} from "./Mutation";
import Button from "@material-ui/core/Button";
import {Delete, Edit} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  }
}));

function EmployeesTable() {
  const handleOpenDialog = (item) => {
    setEmployeeForEdit(item)
    setOpenDialog(true)
  }

  const saveEmployeeHandle = (employee) => {
    if (employee['id'] === '0'){
      addEmployee({variables: {request: {name: employee['name'], position_id: employee['position_id']}},
        refetchQueries: [{query: allEmployeesQuery}]})
    }
    else{
      updateEmployee({
        variables: {
          request: {
            id: employee['id'], name: employee['name'],
            position_id: employee['position_id']
          }
        }, refetchQueries: [{query: allEmployeesQuery}]
      }).then((r) => {console.log(r)})
    }
    setOpenDialog(false)
  }

  const deleteEmployeeHandle = (id) => {
    delEmployee({variables: {id: id}, refetchQueries: [{query: allEmployeesQuery}]})
  }

  const [openDialog, setOpenDialog] = React.useState(0)
  const [addEmployee, {add_data}] = useMutation(addEmployeeMutation)
  const [updateEmployee, {edit_data}] = useMutation(updateEmployeeMutation)
  const [delEmployee, {del_data}] = useMutation(deleteEmployeeMutation)
  const [EmployeeForEdit, setEmployeeForEdit] = React.useState(null)

  const classes = useStyles();
  const { loading, error, data } = useQuery(allEmployeesQuery);
  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
     <div>
       <h1> Сотрудники </h1>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell> id </TableCell>
                <TableCell> ФИО </TableCell>
                <TableCell> Должность </TableCell>
                <TableCell> Действия </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.allEmployees.map(({id, name, position}) => (
                <TableRow key={id}>
                  <TableCell> {id} </TableCell>
                  <TableCell> {name} </TableCell>
                  <TableCell> {position['name']} </TableCell>
                  <TableCell>
                    <Button color="primary" onClick={() => {handleOpenDialog({id: id, name: name,
                    position_id: position["id"]})}}>
                      <Edit/>
                    </Button>
                    <Button color="secondary" onClick={() => {deleteEmployeeHandle(id)}}>
                      <Delete/>
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <Fab color='primary' aria-label='Add' className={classes.fab} onClick={
            () => {handleOpenDialog({id: '0', name:"", position_id: 0})}
          }>
            <AddIcon/>
          </Fab>
          <Popup
            openPopup = {openDialog}
            setOpenPopup = {setOpenDialog}
            title = {'Редактирование сотрудника'}
          >
            <EmployeeForm
              saveHandle = {saveEmployeeHandle}
              employee = {EmployeeForEdit}
            />
          </Popup>
        </div>
      )
}

export default EmployeesTable;
