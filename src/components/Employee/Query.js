import {gql} from 'apollo-boost';

export const allEmployeesQuery = gql `
  query {
    allEmployees {
      id
      name
      position{
        id
        name
      }
    }
  }
`
