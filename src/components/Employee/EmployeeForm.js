import React from "react";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import {useQuery} from "@apollo/react-hooks";
import {allPositionQuery} from "../Position/Query";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

function EmployeeForm(props) {
  const {employee, saveHandle} = props
  const [name, setName] = React.useState(employee["name"]);
  const [position, setPosition] = React.useState(employee["position_id"]);

  const { loading, error, data } = useQuery(allPositionQuery);

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
    <div>
    <form onSubmit={e => {
      e.preventDefault()
      saveHandle({id: employee["id"], name: name, position_id: parseInt(position)})
    }}>
      <p>
        <TextField
          id = 'name'
          label = 'name'
          value = {name}
          onChange={e => {
            setName(e.target.value)
          }}
        >
        </TextField>
      </p>
      <p>
        <Select
        id = 'position'
        label='position'
        value={position}
        onChange={(e) => {
          setPosition(e.target.value)
        }}>
          {data.allPositions.map(({id, name}) => (
            <MenuItem value={id}> {name} </MenuItem>
          ))}
        </Select>
      </p>
      <p>
        <Button
          type="submit"
          color="primary"
          variant="contained">
          Save
        </Button>
      </p>
    </form>
    </div>
  )
}

export default EmployeeForm;
