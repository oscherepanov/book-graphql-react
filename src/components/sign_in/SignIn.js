import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import React from "react";
import {useMutation} from "@apollo/react-hooks";
import {SignInMutation} from "./Mutation";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function SignIn(props) {
  const {setOpenDialog, setAuth} = props
  const [open, setOpen] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState('')

  const [email, setEmail] = React.useState(null)
  const [password, setPassword] = React.useState(null)
  const [signIn,] = useMutation(SignInMutation,
    {
      onError(err) {
        setErrorMessage(err.message)
        setOpen(true)
      },
      onCompleted(data) {
        setAuth({email:email, token: data.signIn.token})
        setOpenDialog(false)
      }
    }
    )

  const handleClose = (event, reason) => {
    setOpen(false)
  };

  return(
    <div>
      <form onSubmit={e => {
        e.preventDefault()
        signIn({variables: {auth: {email: email, password: password}}})
      }}>
        <p>
          <TextField
            id = 'email'
            label = 'email'
            value = {email}
            required
            fullWidth={true}
            onChange={e => {
              setEmail(e.target.value)
            }}
          >
          </TextField>
        </p>
        <p>
          <TextField
            id = 'password'
            label = 'password'
            value = {password}
            type="password"
            required
            fullWidth={true}
            onChange={e => {
              setPassword(e.target.value)
            }}
          >
          </TextField>
        </p>
        <p>
          <Button
            type="submit"
            color="primary"
            variant="contained">
            Save
          </Button>
        </p>
      </form>
      <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="warning">
          {errorMessage}
        </Alert>
      </Snackbar>
    </div>
  )
}

export default SignIn
