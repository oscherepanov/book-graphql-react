import {gql} from "apollo-boost";

export const SignInMutation = gql `
  mutation SignIn($auth: AuthData){
    signIn(auth: $auth){
      token
    }
  }
`
