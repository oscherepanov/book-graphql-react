import './App.css';
import React from "react";
import ApolloClient from "apollo-client";
import {ApolloProvider} from "react-apollo"
import {InMemoryCache} from "apollo-cache-inmemory";
import AppBar from "@material-ui/core/AppBar";
import {Container, Tabs} from "@material-ui/core";
import Tab from "@material-ui/core/Tab";
import AuthorsTable from "./components/Author/AuthorsTable";
import EmployeesTable from "./components/Employee/EmployeesTable";
import ReaderTable from './components/Reader/ReaderTable';
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Popup from "./components/utils/Popup";
import SignUp from "./components/sign_up/SignUp";
import SignIn from "./components/sign_in/SignIn";
import {ApolloLink, split} from "apollo-link";
import useAuth from "./components/utils/UseAuth";
import BookTable from "./components/Book/BookTable";
import JournalTable from "./components/Journal/JournalTable";
import {WebSocketLink} from "apollo-link-ws";
import {getMainDefinition} from "apollo-utilities";
import BookSubscription from "./components/BookSubscription/BookSubscription";
import FileUploadForm from "./components/FileUpload/FileUploadForm";
import {createCustomLink} from "./components/graphql-utils";

const httpLink = new createCustomLink({ uri: 'http://localhost:8080/graphql' });

const authLink = new ApolloLink((operation, forward) => {
  const authData = localStorage.getItem('authData');
  let token = ''
  try {
    token = JSON.parse(authData)['token']
  }
  catch (e){
  }
  operation.setContext({
    headers: {
      "access-token": token,
    }
  });

  return forward(operation);
});


const wsLink = new WebSocketLink({
  uri: `ws://localhost:8080/subscriptions`,
  options: {
    reconnect: true
  }
});

const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    );
  },
  wsLink,
  authLink.concat(httpLink),
);

const client = new ApolloClient({
  link:  link,
  cache: new InMemoryCache()
});

const useStyles = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
  },
}));

const App = () => {
  const classes = useStyles();

  const [openSignInDialog, setOpenSignInDialog] = React.useState(false)

  const [auth, setAuth] = useAuth(null)
  const [openRegDialog, setOpenRegDialog] = React.useState(false)

  const [selectedTab, setSelectedTab] = React.useState(0)

  const handleChange = (event, newValue) => {
    setSelectedTab(newValue)
  }
  return (
    <ApolloProvider client={client}>
      <AppBar position="static">

        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Библиотека
          </Typography>
            <Button color="inherit" onClick={() => {
              setOpenRegDialog(true)
            }}>
              Регистрация
            </Button>
          { !auth && <Button color="inherit" onClick={() => {
              setOpenSignInDialog(true)
            }}>Вход</Button>
          }
          { auth && <Button color="inherit" onClick={() => {
            setAuth(null)
          }}> {auth['email']}</Button>
          }
        </Toolbar>

        <Tabs value={selectedTab} onChange={handleChange}>
          <Tab label="Авторы"/>
          <Tab label="Сотрудники"/>
          <Tab label="Читатели"/>
          <Tab label="Книги"/>
          <Tab label="Журнал"/>
          <Tab label="Подписка на книги"/>
          <Tab label="Загрузка файлов"/>
        </Tabs>
      </AppBar>
      <Container>
        {selectedTab === 0 && <AuthorsTable />}
        {selectedTab === 1 && <EmployeesTable />}
        {selectedTab === 2 && <ReaderTable />}
        {selectedTab === 3 && <BookTable />}
        {selectedTab === 4 && <JournalTable />}
        {selectedTab === 5 && <BookSubscription />}
        {selectedTab === 6 && <FileUploadForm />}

      </Container>

      <Popup
        openPopup = {openRegDialog}
        setOpenPopup = {setOpenRegDialog}
        title = {'Регистрация'}
      >
        <SignUp
          setOpenDialog = {setOpenRegDialog}/>
      </Popup>

      <Popup
        openPopup = {openSignInDialog}
        setOpenPopup = {setOpenSignInDialog}
        title = {'Вход'}
      >
        <SignIn
          setOpenDialog = {setOpenSignInDialog}
          setAuth = {setAuth}
        />
      </Popup>
    </ApolloProvider>
  )
}

export default App;
